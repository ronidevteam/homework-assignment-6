/**
 * Main App File
 */

// Dependencies
const server = require('./server');
const cluster = require('cluster');
const os = require('os');


// Container for the app
const app = {};

app.init = () => {
  if (cluster.isMaster) {
    for (let i = 0; i < os.cpus().length; i++) {
      cluster.fork();
    }
  } else {
    server.init();
  }
};



app.init();